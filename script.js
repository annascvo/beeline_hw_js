//hw 11 Что выведется в результате выполнения следующих выражений? В комментариях просьба отписать, почему получили именно такой результат
//1)
const test = {}

if(test){ 
  console.log('i am here') //получим данную строку в консоли, преобразование объекта в булевый тип. Все объекты -true, даже пустые
} else {
  console.log('now here')
}

//2)
const date = new Date();

console.log(date < []) //false, приведение к числу для сравнения, у Date valueOf() дает количество миллисекунд, а [] дает 0

//3)
const obj = {a: 2, b: 3};
console.log(obj == {a: 2, b: 3}) //false, объекты будут равны, только если указывают на одно место в памяти, а тут разные объекты

//4)
console.log(!{} + 'test') //falsetest, пустой объект всегда true, !true -> false, при сложении приведение к строке и конкатенация

//5)
console.log({} + [1, 2]) //[object Object]1,2, интерпретатор JS пытается преобразовать всё к числам из-за сложения, не получается, заменяет объект строкой [object Object], всё превращает в строки и склеивает их в одну большую, но у массивов ключи склеиваются через запятую в общую строку

//hw12 Выполнить задания и в комментариях пояснить, почему получили такой результат:
//1)
const greetPerson = () => {
  const sayName = (name) => { //2) вызываем функцию sayName('name')
    return `hi, i am ${name}` //3) функция sayName возвращает строку `hi, i am ${name}`
  }
  return sayName //4) функция greetPerson возвращает результат sayName, строку `hi, i am ${name}`
}

const greeting = greetPerson(); //1) вызываем функцию greetPerson('name')
console.log(greeting('Pavel')); //hi, i am Pavel
console.log(greeting('Irina')); //hi, i am Irina

//2)
let y = 'test';
const foo = () => {
  var newItem = 'hello';
  console.log(y);
}
foo(); //1) вывод в консоль test, т.к. переменную y объвили выше вызова функции foo, откуда она и взяла ее значение
console.log(newItem); //2) ошибка newItem is not defined, переменна не была объявлена, но если объявить ее на 42 строке, то она не попадает в область видимости и ошибка останется



//3)
let y = 'test';
let test = 2;
const foo = () => {
  const test = 5; 
  const bar = () => {
    console.log(5 + test); //берем ближайший test = 5, получаем выражение 5+5, в консоль выводим 10
  }
  bar()
}
foo();

//4)
const bar = () => {
  const b = 'no test'
}
bar(); //вызвали функцию bar, где в ее области видимости объявили b = 'no test'


const foo = (() => {
  console.log(b); //пытаемся вывести в консоль b, до ее определения, получаем ошибку Cannot access 'b' before initialization, если перенести строку 73 на 68, получим вывод в консоль test
})();

const b = 'test';


